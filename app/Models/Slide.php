<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    public function getSlideImageAttribute($location){
        return url("storage/".$location);
    }
}
