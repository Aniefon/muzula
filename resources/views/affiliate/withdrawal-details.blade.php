@extends('affiliate.layout.master')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Link Tool</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>



    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{route('update-bank')}}">
                       @csrf
                        <div class="card-body">

                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                            @endif

                            <h4 class="card-title">Bank  Details</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Bank Name</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-control {{$errors->has('bank_id')?'is-invalid':''}}"
                                           name="bank_id"
                                          >
                                        <option value="" disabled selected>Select Bank</option>
                                        @foreach($banks as $bank)
                                        <option value="{{$bank->id}}" {{$bank->id==$user->bank_id?'selected':old('bank_id')==$bank->id?'selected':''}}>{{$bank->name}}</option>
                                            @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        {{$errors->first('bank_name')}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Account Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control {{$errors->has('account_no')?'is-invalid':''}}"
                                           value="{{$user->account_no?$user->account_no:old('account_no')}}" name="account_no" placeholder="Account Number">
                                    <div class="invalid-feedback">
                                        {{$errors->first('account_no')}}
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
            {{--<div class="col-md-6">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-body">--}}
                        {{--<h5 class="card-title m-b-0">Form Elements</h5>--}}
                        {{--<div class="form-group m-t-20">--}}
                            {{--<label>Date Mask <small class="text-muted">dd/mm/yyyy</small></label>--}}
                            {{--<input type="text" class="form-control date-inputmask" id="date-mask" placeholder="Enter Date">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label>Phone <small class="text-muted">(999) 999-9999</small></label>--}}
                            {{--<input type="text" class="form-control phone-inputmask" id="phone-mask" placeholder="Enter Phone Number">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label>International Number <small class="text-muted">+19 999 999 999</small></label>--}}
                            {{--<input type="text" class="form-control international-inputmask" id="international-mask" placeholder="International Phone Number">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label>Phone / xEx <small class="text-muted">(999) 999-9999 / x999999</small></label>--}}
                            {{--<input type="text" class="form-control xphone-inputmask" id="xphone-mask" placeholder="Enter Phone Number">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label>Purchase Order <small class="text-muted">aaaa 9999-****</small></label>--}}
                            {{--<input type="text" class="form-control purchase-inputmask" id="purchase-mask" placeholder="Enter Purchase Order">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


            </div>
        </div>
        <!-- editor -->

    </div>

@stop
