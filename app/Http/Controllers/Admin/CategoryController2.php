<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MainCategoryRequest;
use App\Http\Requests\SubCategoryRequest;
use App\Http\Requests\SubSubCategoryRequest;
use App\Models\MainCategory;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController2 extends Controller
{


    public function __construct(Request $request)
    {
//        $this->request = $request;


    }

    public function showMainCategories(){
        $categories = MainCategory::with(["subCategories","subSubCategories"])->get();



        return response()->json($categories);
    }


    public function transformMainCategory($category){

    }


    public function storeMainCategory(MainCategoryRequest $request){
        $category = new MainCategory();
        $category->category_name = $request->category_name;


        if($category->save()){
            $category->sub_categories = [];
            return $this->success("category added",$category);
        }


        return $this->error("category not added");

    }


    public function updateMainCategory(MainCategoryRequest $request,$id){

        $category =  MainCategory::find($id);
        $category->category_name = $request->category_name;

        if($category->save())
            return $this->success("category updated",$category);

        return $this->error("category not added");
    }


    public function deleteMainCategory($id){
        if(MainCategory::destroy($id))
            return $this->success("category deleted");

        return $this->error("category not deleted");
    }















    public function showSubCategories(){
        $categories = SubCategory::with(["subCategories","mainCategory"])->get();
        return response()->json($categories);
    }




    public function storeSubCategory(SubCategoryRequest $request){
        $category = new SubCategory();
        $category->category_name = $request->category_name;
        $category->main_category_id = $request->main_category_id;


        if($category->save())
            return $this->success("category added",$category);

        return $this->error("category not added");

    }


    public function updateSubCategory(SubCategoryRequest $request,$id){

        $category =  SubCategory::find($id);
        $category->category_name = $request->category_name;
        $category->main_category_id = $request->main_category_id;


        if($category->save())
            return $this->success("category updated",$category);

        return $this->error("category not added");
    }


    public function deleteSubCategory($id){
        if(SubCategory::destroy($id))
            return $this->success("category deleted");

        return $this->error("category not deleted");
    }















    public function showSubSubCategories(){
        $categories = SubSubCategory::with("subCategories")->get();
        return response()->json($categories);
    }



    public function storeSubSubCategory(SubSubCategoryRequest $request){
        $category = new SubSubCategory();
        $category->category_name = $request->category_name;
        $category->sub_category_id = $request->sub_category_id;


        if($category->save())
            return $this->success("category added",$category);

        return $this->error("category not added");

    }


    public function updateSubSubCategory(SubSubCategoryRequest $request,$id){

        $category =  SubSubCategory::find($id);
        $category->category_name = $request->category_name;
        $category->sub_category_id = $request->sub_category_id;


        if($category->save())
            return $this->success("category updated",$category);

        return $this->error("category not added");
    }



    public function deleteSubSubCategory($id){
        if(SubSubCategory::destroy($id))
            return $this->success("category deleted");

        return $this->error("category not deleted");
    }



}
