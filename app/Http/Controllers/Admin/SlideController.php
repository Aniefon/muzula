<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->success('',Slide::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slide = new Slide();

        $slide->action_url = $request->action_url;
        $slide->slide_image = $request->slide_image->store("slides","public");

        if($slide->save())
            return $this->success('',$slide->refresh());

        return $this->error("Cant add slide");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slide =  Slide::find($id);
        $slide->action_url = $request->action_url;


        if($request->hasFile("slide_image")){
            Storage::disk("public")->delete(trim(parse_url($slide->slide_image)["path"],'/'));
            $slide->slide_image = $request->slide_image->store("slides","public");
        }


        if($slide->save())
            return $this->success('Slide Updated successfully',$slide->refresh());

        return $this->error("Cant update slide");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        Storage::disk("public")->delete(trim(parse_url($slide->slide_image)["path"],'/'));

        if($slide->delete()){
            return $this->success("Slide deleted");
        }

        return $this->error("slide not deleted");
    }
}
