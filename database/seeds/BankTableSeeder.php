<?php

use App\Models\Bank;
use Illuminate\Database\Seeder;


class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(Bank::all()->count()<1){
            $banks = json_decode(file_get_contents(database_path('seeds/banks.json')));
            foreach ($banks as $b){
                $bank = new Bank();
                $bank->name = $b->name;
                $bank->slug = $b->slug;
                $bank->code = $b->code;
                $bank->longcode = $b->longcode;
                $bank->gateway = $b->gateway;
                $bank->pay_with_bank = $b->pay_with_bank;
                $bank->active = $b->active;
                $bank->country = $b->country;
                $bank->currency = $b->currency;
                $bank->type = $b->type;
                $bank->save();
            }


        }

    }
}
