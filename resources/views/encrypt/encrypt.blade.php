
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div class="form">
        @if(session()->has("message"))
            <p> {{session("message")}}</p>
        @endif
        <form enctype="multipart/form-data" method="post" action="{{url("encrypt")}}">

            @csrf

            <div class="">
                <label>Document Name</label>
                <input type="text" name="document_name">
            </div>
            <div>
                <label>Enter encryption password</label>
                <input type="password" name="password">
            </div>

            <div>
                <label>Select File To Encrypt</label>
                <input type="file" name="document">
            </div>
            <div>
                <button type="submit">Upload</button>
            </div>
        </form>
    </div>

</body>
</html>