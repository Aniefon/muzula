
<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>Muzula Affiliate | Register</title>
    <!-- Custom CSS -->
    <link href="{{asset('bus/css/style.min.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="main-wrapper">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
        <div class="auth-box bg-dark border-top border-secondary">
            <div id="loginform">
                <div class="text-center p-t-20 p-b-20">
                    <span class="db"><img src="{{asset('affiliate/images/logo.png')}}" alt="logo" /></span>
                </div>
                <!-- Form -->
                <form class="form-horizontal  m-t-20" id="loginform"  method="post" action="{{route('affiliate.register')}}">

                    @csrf
                    <div class="row p-b-30">
                        <div class="col-12">

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-dark" id="basic-addon1"><i class="ti-user"></i></span>
                                </div>
                                <input type="text" value="{{old('first_name')}}"
                                       class="form-control {{$errors->has('first_name')?'is-invalid':''}} form-control-lg"
                                       placeholder="First Name"
                                       name="first_name"
                                        required>
                                <div class="invalid-feedback">
                                   {{$errors->first('first_name')}}
                                </div>
                            </div>


                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  text-dark" id="basic-addon1"><i class="ti-user"></i></span>
                                </div>
                                <input type="text" value="{{old('last_name')}}" class="form-control form-control-lg {{$errors->has('last_name')?'is-invalid':''}}" placeholder="Last Name" name="last_name" required="">
                                <div class="invalid-feedback">
                                    {{$errors->first('last_name')}}
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-dark" id="basic-addon1"><i class="ti-email"></i></span>
                                </div>
                                <input type="text" value="{{old('email')}}" class="form-control form-control-lg {{$errors->has('email')?'is-invalid':''}}" placeholder="Email" name="email" required="">
                                <div class="invalid-feedback">
                                    {{$errors->first('email')}}
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-dark" id="basic-addon1"><i class="ti-mobile"></i></span>
                                </div>
                                <input type="text" value="{{old('phone_no')}}" class="form-control form-control-lg {{$errors->has('phone_no')?'is-invalid':''}}" placeholder="Phone Number" name="phone_no">
                                <div class="invalid-feedback">
                                    {{$errors->first('phone_no')}}
                                </div>
                            </div>




                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-dark" id="basic-addon2"><i class="ti-key"></i></span>
                                </div>
                                <input type="password" class="form-control form-control-lg {{$errors->has('password')?'is-invalid':''}}" placeholder="Password" name="password">

                                <div class="invalid-feedback">
                                    {{$errors->first('password')}}
                                </div>
                            </div>


                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-dark" id="basic-addon2"><i class="ti-key"></i></span>
                                </div>
                                <input type="password" class="form-control form-control-lg {{$errors->has('password_confirmation')?'is-invalid':''}}" placeholder="Confirm Password" name="password_confirmation" required="">

                                <div class="invalid-feedback">
                                    {{$errors->first('password_confirmation')}}
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="row border-top border-secondary">
                        <div class="col-12">
                            <div class="form-group">
                                <div class="p-t-20">
                                    {{--<button class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Lost password?</button>--}}
                                    <button class="btn btn-success float-right" type="submit">Register</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="recoverform">
                <div class="text-center">
                    <span class="text-white">Enter your e-mail address below and we will send you instructions how to recover a password.</span>
                </div>
                <div class="row m-t-20">
                    <!-- Form -->
                    <form class="col-12" action="index.html">
                        <!-- email -->
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
                            </div>
                            <input type="text" class="form-control form-control-lg" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                        <!-- pwd -->
                        <div class="row m-t-20 p-t-20 border-top border-secondary">
                            <div class="col-12">
                                <a class="btn btn-success" href="#" id="to-login" name="action">Back To Login</a>
                                <button class="btn btn-info float-right" type="button" name="action">Recover</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper scss in scafholding.scss -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper scss in scafholding.scss -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- All Required js -->
<!-- ============================================================== -->
<script src="{{asset('bus/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('bus/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('bus/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugin js -->
<!-- ============================================================== -->
<script>

    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
    $('#to-login').click(function(){

        $("#recoverform").hide();
        $("#loginform").fadeIn();
    });
</script>

</body>

</html>
