<?php

namespace App\Http\Controllers\Affiliate;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{


    public function updateBankAccount(Request $request){

        Validator::make($request->all(),[
            'bank_id'=>'required|exists:banks,id',
            'account_no'=>'required|size:10',
        ])->validate();

        $user = auth()->user();

        $user->bank_id = $request->bank_id;
        $user->account_no =$request->account_no;

        $user->save();


        return back()->with(["message"=>"Bank Details updated successfully"]);
    }


    public function changePassword(){

    }


    public function viewProfile(){


    }
}
