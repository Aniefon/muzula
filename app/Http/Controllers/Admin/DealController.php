<?php

namespace App\Http\Controllers\Admin;

use App\Models\FlashDeal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->success("",FlashDeal::with("product.images")->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $deal = new FlashDeal();

       $deal->product_id = $request->product_id;
       $deal->expire_at = $request->expire_at;

       if($deal->save()){
           return $this->success("Deal created",FlashDeal::with("product")->where("id",$deal->id)->first());
       }

       return $this->error("Deal not created");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deal =  FlashDeal::find($id);

        $deal->product_id = $request->product_id;
        $deal->expire_at = $request->expire_at;


        if($deal->save()){
            return $this->success("Deal created",$deal->refresh());
        }

        return $this->error("Deal not created");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(FlashDeal::find($id)->delete()){
            return $this->success("Product removed");
        }

        return $this->error("Something went wrong");
    }
}
