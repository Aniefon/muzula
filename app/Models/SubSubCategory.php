<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{

    public function subCategories(){
        return $this->belongsTo("App\Models\SubCategory");
    }


    public function products(){
        return $this->belongsTo("App\Models\Product");
    }

}
