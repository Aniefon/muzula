<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{url('/')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset("css/app.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("css/swiper.min.css")}}">

    <title> Muzula | Online shopping | Cheap Phones and Computers</title>
</head>
<body>
<div id="app">

</div>
<script src="https://js.paystack.co/v1/inline.js"></script>
<script src="{{asset("js/swiper.min.js")}}"></script>
<script src="{{asset("js/app.js")}}"></script>



<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : 264416151114831,
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.2'
        });

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));






    };







</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

<style>
    #fb-root > div.fb_dialog.fb_dialog_advanced.fb_shrink_active {
        /*right: initial !important;
        left: 18pt;
        z-index: 9999999 !important;*/
    }
    .fb-customerchat.fb_invisible_flow.fb_iframe_widget iframe {
        width: 100px;
        height: 100px;
    }
    .fb_dialog_content{
        width: 100px;
        height: 100px;
    }
</style>

</body>
</html>
