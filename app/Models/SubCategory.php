<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{

    public function mainCategory(){
       return $this->belongsTo("App\Models\MainCategory");
    }


    public function subCategories(){
        return $this->hasMany("App\Models\SubSubCategory");
    }


    public function products(){
        return $this->hasManyThrough("App\Models\Product","App\Models\SubSubCategory","sub_category_id","category_id");
    }


}
