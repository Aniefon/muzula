<?php

namespace App\Http\Controllers\User;

use App\Models\FlashDeal;
use App\Models\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromotionController extends Controller
{

    public function showSlides(){

        return $this->success("",Slide::all());
    }


    public function showFlashDeals(){
        return $this->success("",FlashDeal::with("product.images")->paginate(12));
    }
}
