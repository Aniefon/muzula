<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('affiliate_users',function (Blueprint $table){
            $table->dropColumn("bank_name");
            $table->unsignedBigInteger("bank_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_users',function (Blueprint $table){
            $table->dropColumn("bank_id");
            $table->unsignedBigInteger("bank_name");
        });
    }
}
