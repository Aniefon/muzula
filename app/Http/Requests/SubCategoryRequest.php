<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_name"=>[
                "required",
                Rule::unique("sub_categories")->ignore(request()->id)],
            "main_category_id"=>"required|exists:main_categories,id"
        ];
    }


    public function messages()
    {
       return [
           "category_name.unique"=>"Category name already exists"
       ];
    }
}
