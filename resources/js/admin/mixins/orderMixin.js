import {http,toast} from "../../../utils";

export default {
    name: "Orders",
    data:()=>({
        orders:[],
        showDetailsPane:false,
        currentOrder:{},
        updatingStatus:false,
    }),
    methods:{


        showOrderDetails(order,index) {
            this.currentIndex = index;
            this.showDetailsPane = true;
            this.currentOrder = order;
        },

        changeOrderStatus(status){
            let data = {status,id:this.currentOrder.id};
            this.updatingStatus = true;

            http.post("admin/order/update-status",data)
                .then((res)=>{
                    console.log(this.currentIndex);
                    this.currentOrder = res.data.data;

                    this.$set(this.orders,this.currentIndex,res.data.data);
                    toast.success("Order Status Updated successfully")
                },()=>{
                    toast.error("Oops something went wrong")
                }).finally(()=>{
                this.updatingStatus = false;
            });
        },

    },
    created(){
        this.getOrders();
    }