<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{


    public function listOrders(){

        $orders = Order::with(["customer","orderItems.product.images"])->orderBy("created_at","DESC")->get();


        return $this->success('',$orders);
    }


    public function changeOrderStatus(Request $request){



        $order = Order::with(["customer","orderItems.product"])->where("id",$request->id)->first();

        $order->status = $request->status;


        if($order->save())
            return $this->success("Order status updated",$order);
        return $this->error("Order not updated");


    }
}
