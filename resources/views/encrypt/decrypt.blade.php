<!
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <form action="{{url("decrypt/".$file->id)}}" method="post">
        @csrf
        <p>{{$file->file_name}}</p>

        @if(session()->has("error"))
            <p style="color: red">{{session("error")}}</p>
        @endif
        <div>
            <p>Enter password to decrypt</p>
            <input type="password" name="password">
        </div>

        <div>
            <button type="submit">Download</button>
        </div>
    </form>
</body>
</html>