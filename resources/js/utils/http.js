import axios from 'axios';
import $ from 'jquery';
import AuthManager from "./Auth";

let auth = {};

// if(AuthManager.getToken()){
//     auth = { Authorization: 'Bearer '+AuthManager.getToken() }
// }



const http = axios.create({
    baseURL: 'https://ecommerce.aniefon.dev',
    headers:{
        "X-CSRF-TOKEN":"Bearer "+ $('meta[name="csrf-token"]').attr('content'),
    }
});

http.interceptors.request.use((config)=>{
     config.headers["Authorization"] = 'Bearer '+AuthManager.getToken();
     return config;
});

export {http}
