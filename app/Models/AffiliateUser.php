<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AffiliateUser extends Authenticatable
{


    use Notifiable;
    protected $fillable  = ['first_name',"last_name",'email','phone_no','affiliate_id','password'];


    protected $guard = 'affiliate';

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function affiliateSales(){

        return $this->hasMany(AffiliateSale::class,'affiliate_id','affiliate_id');
    }


    public function getBalance(){
        return $this->affiliateSales()->sum('amount');
    }

}
