import Vue from 'vue'
import Vuex from 'vuex'
import categories from './modules/categories'
import {categoryWatcher} from "./plugins";
import products from "./modules/products";

Vue.use(Vuex)

export default new Vuex.Store({
    plugins:[categoryWatcher],
    modules:{
        categories,
        products
    },
    state: {

    },
    mutations: {

    },
    actions: {

    }
})
