<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{

    public function orderItems(){

        return $this->hasMany("App\Models\OrderItem");
    }


   public function customer(){
        return $this->belongsTo("App\Models\User","customer_id");
   }

   public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->toDateString();
   }


   public function scopeToday(Builder $query){
        return $query->where(DB::raw("date(created_at)"),now()->toDateString());

   }


}
