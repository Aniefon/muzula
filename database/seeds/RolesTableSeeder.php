<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = new Role();
        $role1->role_name ="admin";
        $role1->display_name = "Admin";
        $role1->save();

        $role2 = new Role();
        $role2->role_name ="user";
        $role2->display_name = "User";
        $role2->save();

    }
}
