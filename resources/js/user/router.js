import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Product from "./views/Product";
import Register from "./views/Register";
import Login from "./views/Login";
import Products from "./views/Products";
import Cart from "./views/Cart.vue";
import Account from "./views/Account.vue";
import EditAccount from "./views/EditAccount";
import Checkout from "./views/checkout/Checkout";
import CartSummary from "./views/checkout/CartSummary";
import ShippingInfo from "./views/checkout/ShippingInfo";
import Payment from "./views/checkout/Payment";
import Orders from "./views/Orders";
import AccountView from "./views/AccountView";
import MainCategory from "./views/MainCategory";
import SubCategoryProducts from "./views/SubCategoryProducts";
import CategoryProducts from "./views/CategoryProducts";
import ProductSearch from "./views/ProductSearch";
import ProductDeals from "./views/ProductDeals";
import ChangePassword from "./views/ChangePassword";
import AffiliateProductChecker from "./utils/AffiliateProductChecker";



Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta:{
        title:'Muzula | Online shopping | Cheap Phones and Computers'
      }
    },
    {
      path:'/product/:id/:slug',
      name:'product',
      component:Product,
      beforeEnter: (to, from, next)=> {
       AffiliateProductChecker.checkAffiliateToken(to.params.id,to.query.ref)
        next();
      }
    },
    {
      path:'/products',
      name:'products',
      component:Products
    },

    {
      path:'/m-cat/:id/:slug',
      name:'main_category',
      component:MainCategory
    },

    {
      path:'/s-cat/:id/:slug',
      name:'sub_category',
      component:SubCategoryProducts
    },
    {
      path:'/cat/:id/:slug',
      name:'category',
      component:CategoryProducts
    },

    {
      path:'/search',
      name:'search',
      component:ProductSearch
    },
    {
      path:"/flash-deals",
      name:'flashDeals',
      component:ProductDeals
    },
    {
      path:'/register',
      name:'register',
      component:Register
    },
    {
      path:'/login',
      name:'login',
      component:Login
    },

    {
      path:'/cart',
      name:'cart',
      component:Cart
    },

    {
      path:'/account',
      component:Account,
      children:[
        {
          path:'/',
          component:AccountView
        },
        {
          path:'edit',
          component:EditAccount
        },
        {
          path:'orders',
          component:Orders
        },
        {
          path:'change-password',
          component:ChangePassword
        }
      ],

    },

    {
      path:"/checkout",
      component:Checkout,
      // children: [
      //   {
      //     path:'summary',
      //     component:CartSummary,
      //   },
      //   {
      //     path:'shipping-info',
      //     component:ShippingInfo
      //   },
      //   {
      //     path:'payment',
      //     component:Payment
      //   }
      // ]
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
