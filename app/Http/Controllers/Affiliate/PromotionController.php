<?php

namespace App\Http\Controllers\Affiliate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PromotionController extends Controller
{


    public function generateLink(Request $request){

        Validator::make($request->all(),[
            'url'=>'required|url',
        ])->validate();

        $user = auth()->user();
        $url = trim($request->url,'/,&,?');
        $urlDetails = parse_url($url);
        $siteUrl = parse_url(url('/'));



        if($urlDetails["host"]!=$siteUrl['host']){
            return back()->withErrors(["url"=>"The url you entered is invalid"]);
        }

       $generateUrl=$url;

        if(preg_match('/\?/',$url)>0){
            $generateUrl=$url.'&ref='.$user->affiliate_id;
        }else{
            $generateUrl=$url.'?ref='.$user->affiliate_id;
        }




        return back()->with(["generated-link"=>$generateUrl]);



    }
}
