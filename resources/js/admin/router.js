import Vue from 'vue'
import Router from 'vue-router'
import Categories from './views/categories/Categories'
import ProductList from "./views/products/ProductList";
import NewProducts from "./views/products/NewProducts";
import Orders from "./views/orders/Orders";
import Customers from "./views/customers/Customers";
import Slides from "./views/slides/Slides";
import FlashDeals from "./views/deals/FlashDeals";
import Admin from './views/admin/Admin'
import Login from "./views/login/Login";
import DashboardLayout from "./views/dashboard-layout/DashboardLayout";
import DashboardSummary from "./views/dashboard-summary/DashboardSummary";

import Auth from '../utils/Auth'
import ChangePassword from "./views/change-password/ChangePassword";
// import Home from './views/Home.vue'
// import Product from "./views/Product";
// import Register from "./views/Register";
// import Login from "./views/Login";
// import Products from "./views/Products";

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: "/admin",
    routes: [
        {
            path:'/',
            name:'admin',
            component:Admin,
            children:[
                {
                    path:'/login',
                    component:Login
                },

                {
                    path:'/',
                    redirect:'/login',
                    component:DashboardLayout,
                    beforeEnter: (to, from, next) => {
                        if(!Auth.userIsLogged())
                            next('/login?backto='+to.fullPath);
                        else
                            if(Auth.getUser().role.role_name!=='admin')
                                next('/login?backto='+to.fullPath);
                            else
                              next();
                    },
                    children: [
                        {
                            path:'dashboard',
                            component:DashboardSummary
                        },
                        {
                            path:'products',
                            name:'products',
                            component:ProductList
                        },
                        {
                            path:'/categories',
                            name:"categories",
                            component:Categories
                        },
                        {
                            path:'/products',
                            name:'products',
                            component:ProductList
                        },
                        {
                            path:"/products/create",
                            name:"create_product",
                            component:NewProducts
                        },

                        {
                            path:'/products/edit/:id',
                            name:"edit_product",
                            component:NewProducts
                        },
                        {
                            path:'/orders',
                            name:'orders',
                            component:Orders
                        },
                        {
                            path:'/customers',
                            name:'customers',
                            component:Customers
                        },
                        {
                            path:'/slides',
                            name:'slides',
                            component:Slides
                        },
                        {
                            path:'/deals',
                            name:'deals',
                            component:FlashDeals
                        },
                        {
                            path:'/change-password',
                            name:'change-password',
                            component:ChangePassword
                        }
                    ]
                }
            ]

        },


        // {
        //     path:'/categories',
        //     name:"categories",
        //     component:Categories
        // },
        // {
        //     path:'/products',
        //     name:'products',
        //     component:ProductList
        // },
        // {
        //     path:"/products/create",
        //     name:"create_product",
        //     component:NewProducts
        // },
        //
        // {
        //     path:'/products/edit/:id',
        //     name:"edit_product",
        //     component:NewProducts
        // },
        // {
        //     path:'/orders',
        //     name:'orders',
        //     component:Orders
        // },
        // {
        //     path:'/customers',
        //     name:'customers',
        //     component:Customers
        // },
        // {
        //     path:'/slides',
        //     name:'slides',
        //     component:Slides
        // },
        // {
        //     path:'/deals',
        //     name:'deals',
        //     component:FlashDeals
        // }
        // {
        //   path: '/',
        //   name: 'home',
        //   component: Home
        // },
        // {
        //   path:'/product',
        //   name:'product',
        //   component:Product
        // },
        // {
        //   path:'/products',
        //   name:'products',
        //   component:Products
        // },
        // {
        //   path:'/register',
        //   name:'register',
        //   component:Register
        // },
        // {
        //   path:'/login',
        //   name:'login',
        //   component:Login
        // },
        // {
        //   path: '/about',
        //   name: 'about',
        //   // route level code-splitting
        //   // this generates a separate chunk (about.[hash].js) for this route
        //   // which is lazy-loaded when the route is visited.
        //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        // }
    ]
})
