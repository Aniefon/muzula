<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AccountController extends Controller
{


    public function login(LoginRequest $request){
        $token = auth()->attempt($request->only(["email","password"]));

        if(!$token){
            return $this->error("Invalid username or password",null,401);
        }

        $user = User::with('role')->where("email",$request->email)->first();
        return $this->success("login was successful",compact("user","token"));
    }







    public function updateAccount(UpdateAccountRequest $request){

        $userId = auth()->user()->id;

        $user = User::find($userId);


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
//       $user->email = $request->email;
        $user->phone_no = $request->phone_no;




        $customer = Customer::updateOrCreate([
            'user_id'=>$userId
        ],[
            "address"=>$request->address,
        ]);


        if( $user->save()){
            return $this->success("Account Updated successfully", compact("user","customer"));
        }

    }


    public function logout(){

        $token = JWTAuth::parseToken();

        $token->invalidate();

        return $this->success("logged out successfully");
    }

}
