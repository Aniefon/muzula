<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\AddressRequest;
use App\Models\CustomerAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{


    public function listAddress(){

        return $this->success("",CustomerAddress::all());
    }


    public function addAddress(AddressRequest $request){

        $customer = auth()->user();
        $address = new CustomerAddress();
        $address->customer_id = $customer->id;
        $address->first_name = $request->first_name;
        $address->last_name = $request->last_name;
        $address->phone_no = $request->phone_no;
        $address->phone_no2 = $request->phone_no2;
        $address->state = $request->state;
        $address->lga = $request->lga;
        $address->additional_info = $request->additional_info;
        $address->address = $request->address;


        if($address->save()){
             return $this->success("Address added",$address->refresh());
        }

        return $this->error("Address could not be added");

    }



    public function updateAddress(AddressRequest $request,$id){


        $address =  CustomerAddress::find($id);
        $address->first_name = $request->first_name;
        $address->last_name = $request->last_name;
        $address->phone_no = $request->phone_no;
        $address->phone_no2 = $request->phone_no2;
        $address->state = $request->state;
        $address->lga = $request->lga;
        $address->additional_info = $request->additional_info;
        $address->address = $request->address;


        if($address->save()){
            return $this->success("Address updated",$address->refresh());
        }

        return $this->error("Address could not be update");

    }



    public function deleteAddress($id){

        if(CustomerAddress::destroy($id)){
            return $this->success("Address deleted");
        }

        return $this->error("Address not deleted");
    }
}
