import {http,toast} from "../../utils";

export default {

    data:()=>({
        editingAddress:false,
        editedAddress:null,
    }),

    methods:{

        openModal(){
            $("#address-modal").modal("show");
        },

        addAddress(address){
            this.$store.commit("addAddress",address);
        },

        deleteAddress(index,id){
            if(confirm("Do you want to delete address")){
                http.delete("user/delete-address/"+id)
                    .then((res)=>{
                        this.$store.commit("deleteAddress",index);
                        toast.success("Address deleted successfully")
                    },()=>{
                        toast.error("Oops something went wrong");
                    })

            }
        },

        addressUpdated(address){
            this.$store.commit("updateAddress",{index:this.currentIndex,address})
        },

        clearEditing(){
            this.editingAddress = false;
            this.editedAddress = null;
        },

        editAddress(index){

            this.editedAddress = this.user.addresses[index];
            this.editingAddress = true;
            this.currentIndex = index;
            this.$nextTick(()=>{
                $("#address-modal").modal("show");
            })

        },
    },
    computed:{
        user(){
            return this.$store.state.user;
        }
    }
}