<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $user = $request->user();

        if(!$user){
            return response()->json(["message"=>"unauthorized"],405);
        }else{
            if($user->role->name !='admin'){
                return response()->json(["message"=>"not an admin"],405);

            }
        }

        return $next($request);
    }
}
