var days =  29;

(function (ctx) {

var dateLabels = generateDates();
var views = generateViews();
var sales = generateSales();
    new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: dateLabels,//['Jan 1', 'Jan 1', 'Jan 1', 'Jan 1', 'Jan 1', 'Jan 1', 'Jan 1','Jan 1','Jan 1','Jan 1','Jan 1','Jan 1','Jan 1','Jan 1'],

            datasets: [
                {
                    label: 'Views',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    fill:false,
                    data: views//[2000, 45,200, 1244, 0, 0,0 ]
                },

                {
                    label: 'Sales',
                    fill:false,
                    backgroundColor: 'rgb(200, 199, 132)',
                    borderColor: 'rgb(200, 199, 132)',
                    data: sales
                }
            ]
        },

        // Configuration options go here
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 45,
                        // minRotation: 90
                    }
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: 1
                    }
                }]
            }
        }
    });
})(document.getElementById('myChart').getContext('2d'));




function generateDates() {


    var startDate = moment().subtract(days,'days');


    var dates = [];

    for (var i=0; i<=days; i++){
        dates.push(startDate.format('ddd, MMM D'))
        startDate.add(1,'days')
    }


    return dates;

}



function generateViews() {

    var dates = generateDates();
    var views = chartData.views //Coming from views/affiliate/dashboard

    var viewsData = new Array(days+1);
    viewsData.fill(0,0);

    for (var i=0; i<days; i++){

        if(views[i]){
            viewsData[i] = Number.parseInt(views[i].views);
        }else {
            viewsData[i] = 0;
        }
    }

   return viewsData.reverse();
}


function generateSales() {

    var dates = generateDates();
    var salesData = new Array(days+1);
    salesData.fill(0,0);

    var sales = chartData.sales; ////Coming from views/affiliate/dashboard


    sales.forEach(function (sale) {
        var index = dates.findIndex(function (date) {
            return moment(sale.date).format('ddd, MMM D') ==date
        });

        if(index!=-1){
            salesData[index] = sale.sales;
        }
    });


    return salesData;


}




































































// (function() {
//     var offset = 0;
//     var sin = []
//         , cos = [];
//     for (var i = 0; i < 12; i += 0.2) {
//         sin.push([i, Math.sin(i + offset)]);
//         cos.push([i, Math.cos(i + offset)]);
//     }
//
//
//     var options = {
//         series: {
//             lines: {
//                 show: true
//             }
//             , points: {
//                 show: false
//             }
//         }
//         , grid: {
//             hoverable: true //IMPORTANT! this is needed for tooltip to work
//         }
//         // , yaxis: {
//         //     min: -1.2
//         //     , max: 1.2
//         // }
//         , colors: ["#ee7951", "#4fb9f0"]
//         , grid: {
//             color: "#AFAFAF"
//             , hoverable: true
//             , borderWidth: 0
//             , backgroundColor: '#FFF'
//         }
//         , tooltip: true
//         , tooltipOpts: {
//             content: "'%s' of %x.1 is %y.4"
//             , shifts: {
//                 x: -60
//                 , y: 25
//             }
//         }
//     };
//
//
//
//     var plotObj = $.plot($("#flot-line-chart"),
//         [
//             {
//                 data:[[1, 3], [2, 14.01], [3.5, 3.14]],
//                 label:'Clicks'
//             },
//             {
//                 data:[],
//                 label:'Pending orders'
//             },
//             {
//                 data:[],
//                 label:'Confirmed Orders'
//             }
//         ], options);
// })()
