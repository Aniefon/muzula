<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    public function create(Request $request){

        $category = new Category();

        $category->parent_id = $request->has("parent_id")?$request->parent_id:null;
        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->affiliate_commission = $request->affiliate_commission??0;

        if($request->hasFile("category_image")){
            $request->category_image = $request->category_image->store("category","public");
        }

        if($category->save()){
            return $this->success("Category added",$category);
        }

        return $this->error("Category not added");
    }


    public function update(Request $request,$id){

        $category =  Category::find($id);

        $category->parent_id = $request->has("parent_id")?$request->parent_id:null;
        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->affiliate_commission = $request->affiliate_commission??0;

        if($request->hasFile("category_image")){
            $request->category_image = $request->category_image->store("category","public");
        }

        if($category->save()){
            return $this->success("Category updated",$category);
        }

        return $this->error("Category not updated");
    }


    public function list(){
        $categories = Category::with("subCategories.subCategories")->where("parent_id")->get();
        return $this->success("",$categories);
    }


    public function listMain(){

        $categories = Category::where("parent_id",null)->get();

        return $this->success("",$categories);
    }

    public function listSub(){
//        $categories = DB::table("categories")
//            ->
    }

    public function listCategories(){

    }


    public function delete($id){

        if(Category::destroy($id))
            return $this->success("Category deleted");

        return $this->error("Category not deleted");

    }


}
