<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/






Route::domain('api.muzula.com')->group(function (){
    loadRoutes();
});




loadRoutes();



function loadRoutes(){
    Route::group(["prefix"=>"admin","namespace"=>"Admin"],function (){
        Route::post("login","AccountController@login");

        Route::get('/summary','DashboardController@dashboardSummary');

        Route::prefix("categories")->group(function () {
            Route::post("create","CategoryController@create");
            Route::get("list","CategoryController@list");
            Route::post("update/{id}","CategoryController@update");
            Route::delete("delete/{id}","CategoryController@delete");



            Route::prefix("main-category")->group(function(){
                Route::get("list","CategoryController@showMainCategories");
                Route::post("create","CategoryController@storeMainCategory");
                Route::post("update/{id}","CategoryController@updateMainCategory");
                Route::delete("delete/{id}","CategoryController@deleteMainCategory");
            });






            Route::prefix("sub-category")->group(function(){
                Route::get("list","CategoryController@showSubCategories");
                Route::post("create","CategoryController@storeSubCategory");
                Route::post("update/{id}","CategoryController@updateSubCategory");
                Route::delete("delete/{id}","CategoryController@deleteSubCategory");
            });

            Route::prefix("sub-sub-category")->group(function(){
                Route::get("list","CategoryController@showSubSubCategories");
                Route::post("create","CategoryController@storeSubSubCategory");
                Route::post("update/{id}","CategoryController@updateSubSubCategory");
                Route::delete("delete/{id}","CategoryController@deleteSubSubCategory");
            });
        });


        Route::prefix("products")->group(function (){
            Route::get("/list","ProductController@index");

            Route::get("/","ProductController@index");
            Route::get("/{product}","ProductController@show");
            Route::post("create","ProductController@store");
            Route::post("update/{id}","ProductController@update");
            Route::delete("delete/{id}","ProductController@destroy");

        });

        Route::prefix("customers")->group(function (){
            Route::get('list','CustomerController@listCustomers');
        });

        Route::prefix("order")->group(function (){
            Route::get("list","OrderController@listOrders");
            Route::post("update-status","OrderController@changeOrderStatus");

        });


        Route::prefix("promotion")->group(function (){
            Route::get("slide/list","SlideController@index");
            Route::post("slide/add","SlideController@store");
            Route::post("slide/update/{id}","SlideController@update");
            Route::delete("slide/delete/{id}","SlideController@destroy");


            Route::get("deal/list","DealController@index");
            Route::post("deal/add","DealController@store");
            Route::delete("deal/delete/{id}","DealController@destroy");
        });

    });



    Route::namespace("User")->group(function (){
        Route::post('order/new',"OrderController@createOrder")->middleware("auth:api");
        Route::get("order/list","OrderController@listOrders")->middleware("auth:api");

        Route::prefix("promotion")->group(function (){
            Route::get("slides","PromotionController@showSlides");
            Route::get("deals","PromotionController@showFlashDeals");
        });

        Route::prefix("user")->middleware("auth:api")->group(function (){
            Route::post("add-address","CustomerController@addAddress");
            Route::post("update-address/{id}","CustomerController@updateAddress");
            Route::delete("delete-address/{id}","CustomerController@deleteAddress");
        });


    });





    Route::prefix("products")->namespace("User")->group(function (){
        Route::get("categories","ProductController@listCategories");
        Route::get("/search","ProductController@searchProducts");
        Route::get("/main-category/{category}","ProductController@getMainCategoryProducts");
        Route::get("/sub-category/{category}","ProductController@getSubCategoryProducts");
        Route::get("/category/{category}","ProductController@getCategoryProducts");

        Route::get('/{product}',"ProductController@getProduct");

    });

    Route::namespace('User')->prefix('account')->group(function(){
        Route::post('login',"AccountController@login");
        Route::post('register',"AccountController@register");
        Route::post("update",'AccountController@updateAccount');
        Route::post("change-password",'AccountController@updatePassword');
        Route::post('logout',"AccountController@logout")->middleware('auth:api');


    });




    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
}
