export default {


    getToken(){
        // console.log(localStorage.getItem("auth-token"))
        return localStorage.getItem("auth-token")
    },


    storeToken(token){
        localStorage.setItem("auth-token",token)
    },


    deleteToken(){
        localStorage.removeItem("auth-token")
    },

    storeUser(user){
        localStorage.setItem("user",JSON.stringify(user))
    },

    getUser(){
        return JSON.parse(localStorage.getItem("user"))
    },

    loginUser(user,token){
        this.storeToken(token);
        this.storeUser(user);
    }
}