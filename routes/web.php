<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//Route::get('/', function () {
//    return view('welcome');
//});

//



Route::group(["prefix"=>"admin"],function (){
    Route::get("/{path?}",function (){
       return view("admin");
    })->where("path",".*");
});


Route::group(["prefix"=>"affiliate","namespace"=>"Affiliate"],function (){

    Route::get('/',function (){
        return redirect(route('affiliate.login'));
    });
    Route::get('/login',"Auth\LoginController@showLoginForm")->name('affiliate.show-login');
    Route::post('/login',"Auth\LoginController@login")->name('affiliate.login');

    Route::get('/register',"Auth\RegisterController@showRegistrationForm")->name('affiliate.show-register');
    Route::post('/register',"Auth\RegisterController@register")->name('affiliate.register');

    Route::middleware(['web','auth:affiliate'])->group(function (){
        Route::get('/dashboard',"AffiliateController@showDashboard")->name('affiliate.dashboard');
        Route::get('/link-tool',"AffiliateController@showLinkTool")->name('affiliate.link-tool');
        Route::get('banners','AffiliateController@showBanners')->name('affiliate.banners');
        Route::get('withdrawal-details','AffiliateController@showWithdrawalDetails')->name('affiliate.withdrawal-details');
        Route::get('withdrawal-history','AffiliateController@showWithdrawalHistory')->name('affiliate.withdrawal-history');

        Route::post('/generate-link','PromotionController@generateLink')->name('affiliate.generate-link');
        Route::post('/account/update-bank','AccountController@updateBankAccount')->name('update-bank');
    });


});


Route::get('/{path}', function () {
        return view('index');
})->where("path",'.*');
