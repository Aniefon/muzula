<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MainCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_name"=>[
                "required",
                Rule::unique("main_categories")->ignore(request()->id)]
        ];


    }

    public function messages()
    {
        return [
           "category_name.unique"=>"Category name already exist"
        ];
    }
}
