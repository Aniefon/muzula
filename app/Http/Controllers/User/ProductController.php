<?php

namespace App\Http\Controllers\User;

use App\Models\AffiliateProductView;
use App\Models\AffiliateUser;
use App\Models\Category;
use App\Models\MainCategory;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{


    public function listCategories(){

        return $this->success("",Category::with("subCategories.subCategories")->where("parent_id",null)->get());
    }



    public function getMainCategoryProducts($id){
        $products = Product::with("images")->ofMainCategory($id)->paginate(12);
        return $this->success(null,$products);

    }

    public function getCategories(){
        $categories = MainCategory::with("subCategories.subCategories.subCategories")->get();
        return $this->success(null,$categories);
    }


    public function getProduct(Product $product){
        $product->increment("views");
        $product->save();

        if(\request()->has('promoter')){
            $this->increaseAffiliateProductView(\request('promoter'),$product->id);
        }

        $product->images = ProductImage::where("product_id",$product->id)->get();
        return $this->success(null,$product);

    }


    public function increaseAffiliateProductView($affiliateId,$productId){
        $affiliate = AffiliateUser::where('affiliate_id',$affiliateId)->first();

        if($affiliate){
            $views = AffiliateProductView::where("affiliate_id",$affiliateId)
                ->where('product_id',$productId)
                ->where(DB::raw('date(created_at)'),now()->toDateString())
                ->where('affiliate_id',$affiliateId)->first();


            if($views){
                $views->increment('views');
            }else{
                $views = new AffiliateProductView();
                $views->affiliate_id = $affiliateId;
                $views->product_id = $productId;
                $views->viewed_date = now()->toDateString();
                $views->views =1;
            }

            $views->save();

        }
    }


    public function getSubCategoryProducts($id){

        $products = Product::with("images")->ofSubCategory($id)->paginate(12);
        return $this->success(null,$products);
    }



    public function getCategoryProducts($id){
        $products = Product::with("images")->where("category_id",$id)->paginate(12);
        return $this->success(null,$products);
    }


    public function searchProducts(Request $request){

        $products = Product::with("images")->ofProductName($request->search)->paginate(12);
        return $this->success("",$products);
    }
}
