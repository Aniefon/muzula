<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("customer_id");
            $table->boolean("is_primary")->default(true);
            $table->string("first_name");
            $table->string("last_name");
            $table->string("phone_no");
            $table->string("phone_no2")->nullable();
            $table->string("state")->nullable();
            $table->string("lga")->nullable();
            $table->string("additional_info")->nullable();
            $table->string("address")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_addresses');
    }
}
