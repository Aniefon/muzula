import Cookies from 'js-cookie'
export default {

    checkAffiliateToken(productId,affiliateId){
        if(affiliateId){
            Cookies.set('promoter',affiliateId, {path:window.location.pathname, expires: 30 })
        }
    },

    getPromoter(){
        return Cookies.get('promoter')
    }
}