
const categoryWatcher = (store)=>{

    store.subscribeAction({
        before:(action,state)=>{

        },
        after:(action,status)=>{
            switch (action.type) {
                case "updateMainCategory":
                    store.commit("clearArray","subCategories");
                    store.dispatch("getSubCategories");
                    break;
                case "updateSubCategory":
                    store.commit("clearArray","subSubCategories");
                    store.dispatch("getSubSubCategories");
                    break;
            }
        }
    })

};


export {categoryWatcher};