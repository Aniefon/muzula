<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function success($message,$data=null){
        return response()->json(["status"=>"success","message"=>$message,"data"=>$data]);
    }


    public function error($message,$data=null, $code = 500){
        return response()->json(["status"=>"error","message"=>$message,"data"=>$data],$code);
    }
}
