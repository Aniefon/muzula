<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\OrderRequest;
use App\Models\AffiliateSale;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class OrderController extends Controller
{


    public function createOrder(OrderRequest $request){
        $totalAmount = 0;
      $orderItems =   json_decode(json_encode($request->cart));

      collect($orderItems)->each(function ($item) use (&$totalAmount){
          if($item->product->discount_price)
              $totalAmount+=$item->product->discount_price*$item->qty;
          else
              $totalAmount+=$item->product->selling_price*$item->qty;
      });

      $userId = auth()->user()->id;

      $order = new Order();
      $order->customer_id = $userId;
      $order->amount = $totalAmount;
      $order->shipping_cost = $request->shipping_cost;
      $order->address_id = $request->address;
      $order->payment_ref = $request->payment_ref;


      if($order->save()){
          $this->addOrderedItems($order->id,collect($orderItems));
          return $this->success("Order Created",$order->refresh());
      }

      return $this->error("Order not created");

    }


    private function addOrderedItems($orderId,Collection $items){

        $items->each(function ($item) use ($orderId){
            $orderItem = new OrderItem();
            $orderItem->order_id = $orderId;
            $orderItem->product_id = $item->product->id;
            $orderItem->qty = $item->qty;

            if($item->product->discount_price)
                  $orderItem->amount = $item->product->discount_price;
            else
                $orderItem->amount = $item->product->selling_price;
            $orderItem->save();

            $product = Product::find($item->product->id);

            $product->decrement("available_qty");
            $product->save();

            $this->creditPromoter($item,$orderId);
        });

    }

    public function creditPromoter($item,$orderId){

       if(property_exists($item,'promoter')){
           $product = Product::find($item->product->id);
           $commission = $product->category->affiliate_commission;

           if($commission>0){
               $affiliate = new AffiliateSale();
               $affiliate->affiliate_id = $item->promoter;
               $affiliate->order_id = $orderId;
               $affiliate->product_id = $product->id;

               if($item->product->discount_price)
                   $affiliate->amount = ($item->product->discount_price*($commission/100))*$item->qty;
               else
                   $affiliate->amount = ($item->product->selling_price*($commission/100))*$item->qty;

               $affiliate->save();

           }


       }




    }



    public function listOrders(){
        $userId = auth()->user()->id;
        $orders = Order::where("customer_id",$userId)->with("orderItems.product.images")->orderBy("created_at","DESC")->get();

        return $this->success("",$orders);
    }


    public function verifyPayment(){


    }


}
