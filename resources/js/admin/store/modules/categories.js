import {http} from "../../../utils";
import {categoryWatcher} from "../plugins";

export default {

    state:{
        mainCategories:[],
        subCategories:[],
        subSubCategories:[],

        categories:[],

    },



    mutations:{
        addCategory(state,categories){
            let mainCat = [];
            let subCat = [];
            let subSubCat = [];
            for(let main of categories){
                  for(let sub of main.sub_categories){
                      for (let cat of sub.sub_categories){
                          cat.parent = sub.category_name;
                          subSubCat.push(cat);
                      }
                      delete sub['sub_categories'];
                      sub.parent = main.category_name;
                      subCat.push(sub);
                  }
                  // delete main["sub_categories"];
                  mainCat.push(main);
            }

            state.mainCategories = mainCat;
            state.subCategories = subCat;
            state.subSubCategories = subSubCat;


        },


        deleteMainCategory(state, index) {
           state.mainCategories.splice(index,1);
        },


        updateMainCategory(state,updateData){
            state.mainCategories.splice(updateData.index,1,updateData.category);
        },



    },

    actions:{
        getCategories(context){
            http.get("/admin/categories/list")
                .then((res)=>{
                    context.commit("addCategory",res.data.data);
                });
        },


        addCategory(context,category){
            return new Promise((resolve,reject)=>{
                http.post("admin/categories/create",category)
                    .then((res)=>{
                        context.dispatch("getCategories");
                        resolve(res.data);
                    },(err)=>{
                        reject(err.response)
                    });
            })
        },

        updateCategory(context,updateData){
            return new Promise((resolve,reject)=>{
                http.post("admin/categories/update/"+updateData.category.id,updateData.category)
                    .then((res)=>{
                        context.dispatch("getCategories");
                        resolve(res.data);
                    },(err)=>{
                        reject(err.response)
                    });
            })
        },


        deleteCategory(context,id){

            return new Promise((resolve,reject)=>{

                http.delete("admin/categories/delete/"+id)
                    .then((res)=>{
                        context.dispatch("getCategories");
                        resolve(res)
                    },(err)=>{
                        reject(err.response)
                    })
            })


        }



    },



}