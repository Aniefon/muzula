import slugify from '@sindresorhus/slugify';
export default {

    methods:{
        getMainUrl(cat){
            return '/m-cat/'+cat.id+'/'+slugify(cat.category_name.toLowerCase())
        },

        getSubUrl(cat){
            return '/s-cat/'+cat.id+'/'+slugify(cat.category_name.toLowerCase())
        },

        getCategoryUrl(cat){
            return '/cat/'+cat.id+'/'+slugify(cat.category_name.toLowerCase())
        },
    }
}