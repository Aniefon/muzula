<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{



    public function subCategories(){
        return $this->hasMany("App\Models\Category","parent_id");
    }


    public function products(){
        return $this->hasMany("App\Models\Product","category_id");
    }

    public function parent(){
        return $this->belongsTo("App\Models\Category","parent_id");
    }


}
