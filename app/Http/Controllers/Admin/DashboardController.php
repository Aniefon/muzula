<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{


    public function dashboardSummary(){

        $data["total_orders"]  = Order::count();
        $data["sales_amount"] = Order::sum('amount');
        $data['total_customers'] = User::where('role_id',Role::where("role_name",'user')->first()->id)->count();
        $data["recent_orders_count"] = Order::whereBetween(DB::raw('date(created_at)'),[now()->toDateString(),now()->addDay(7)->toDateString()])->count();

        $data['recent_orders'] = Order::with('customer')->orderBy("created_at","DESC")->take(10)->get();

        return $this->success('',$data);

    }

}
