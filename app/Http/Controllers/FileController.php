<?php

namespace App\Http\Controllers;

use App\EncryptedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    private $IV ="fjevnjfejhe78534";
    private $ALGORITHM = 'AES-128-CBC';

    public function encrypt(Request $request){
        $uploaded = file_get_contents($request->document);
        $password = $request->password;
        $encryptedFile = $this->encryptFile($uploaded,$password);
        $ext = $request->document->extension();
        $newFileName = str_random().'.'.$ext;




        Storage::disk("public")->put($newFileName, $encryptedFile);


        $file = new EncryptedFile();

        $file->file_name = $request->document_name.'.'.$ext;
        $file->file_location = $newFileName;

        $file->save();



        return back()->with(["message"=>"File Uploaded successfully"]);



//        $encF = Storage::get("b2ba2d2b96fbd792334099e6e2cf5e45.jpg.crypto");
//        $deF = $this->decryptFile($encF,"secret");
//
//        if(!$deF){
//            return "Invalid Password";
//        }else{
//
//            return response()->streamDownload(function () use ($deF){
//                 echo ($deF);
//            },"Kali.jpg");
//        }


//        print $encF;

    }


    public function decrypt(Request $request,EncryptedFile $encryptedFile){
        $enFile = Storage::disk("public")->get($encryptedFile->file_location);

        $deFile = $this->decryptFile($enFile,$request->password);

        if(!$deFile){
            return back()->with(["error"=>"Invalid Password"]);
        }else{
            return response()->streamDownload(function () use ($deFile){
                 echo ($deFile);
            },$encryptedFile->file_name);
        }

    }




    public function showFiles(){

        $files = EncryptedFile::all();

        return view("encrypt.uploaded",["files"=>$files]);


    }


    public function showFile(EncryptedFile $encryptedFile){

        return view("encrypt.decrypt",["file"=>$encryptedFile]);
    }


    private function encryptFile($fileContent,$password){

        return openssl_encrypt($fileContent,$this->ALGORITHM,$password,0,$this->IV);
    }


    private function decryptFile($fileContent,$password){
        return openssl_decrypt($fileContent,$this->ALGORITHM,$password,0,$this->IV);
    }



}
