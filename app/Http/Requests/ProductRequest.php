<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           "product_name"=>'required',
            "cost_price"=>"required|numeric",
            "selling_price"=>"required|numeric|gte:cost_price",
//            "discount_price"=>"numeric|lt:selling_price",
//            "primary_image"=>"required|image",
            "category_id"=>"required|exists:categories,id",
            "description"=>"required"
        ];
    }
}
