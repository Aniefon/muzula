<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MainCategory extends Model
{


    public function subCategories(){
        return $this->hasMany("App\Models\SubCategory");
    }


    public function subSubCategories(){
        return $this->hasManyThrough("App\Models\SubSubCategory","App\Models\SubCategory","main_category_id","sub_category_id");
    }




    public function products(){
//        $products = SubCategory::hydrate($this->subCategories->toArray());

        return $this->hasManyThrough("App\Models\SubSubCategory","App\Models\SubCategory","main_category_id","sub_category_id")
            ->join("products",'sub_sub_categories.id',"=","products.category_id")
            ->select("products.*");



//       var_dump($products);
//        return $this->subCategories->toArray();
//        return $this->subSubCategories()->get();
//
//        $page = request()->page;
//        if(!$page)
//            $page =1;
//
//
//       return DB::table("products")
//           ->select("*")
//           ->whereRaw("category_id in  (SELECT id FROM sub_sub_categories WHERE  sub_category_id in (SELECT id FROM sub_categories where main_category_id = ?))",[$this->id])
//           ->get()->map(function ($product){
//
//               $product->images = ProductImage::where("product_id",$product->id)->pluck("file_location");
//                 $product->primary_image =$product->images->toArray()[0] ;
//               return $product;
//           })->forPage($page,16);
//
    }
}
