

export default {
    state:{
        cartItems:[],
    },

    mutations:{
        addToCart(state,item){


            let items = state.cartItems;

            for(let i=0; i<items.length; i++){
                if(items[i].product.id == item.product.id){
                    items[i].qty = parseInt( items[i].qty);
                     items[i].qty+=1;

                     localStorage.setItem('cart',JSON.stringify(items));
                     return;
                }
            }
            items.push(item);
            localStorage.setItem('cart',JSON.stringify(items));

        },


        increaseQty(state,item){
            let items = state.cartItems;
            for(let i=0; i<items.length; i++){
                if(items[i].product.id === item.product.id){
                    items[i].qty = parseInt( items[i].qty);
                    items[i].qty = item.qty;
                    localStorage.setItem('cart',JSON.stringify(items));
                  break;
                }
            }
        },

        removeItem(state,index){
            state.cartItems.splice(index,1);

            localStorage.setItem('cart',JSON.stringify(state.cartItems));
        },

        clearCart(state){
            state.cartItems.splice(0,state.cartItems.length);
            localStorage.removeItem("cart");
        },



        getCartItems(state){
            let cart = localStorage.getItem('cart');

            if(cart){
                state.cartItems = JSON.parse(cart);
            }
        }
    },

    actions:{

    },

    getter:{


    }


}
