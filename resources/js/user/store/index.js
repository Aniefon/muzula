import Vue from 'vue'
import Vuex from 'vuex'
import  categories from './modules/categories'
import cart from './modules/cart'
import {http} from "../../utils";
import Auth from '../../utils/Auth'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loggedIn:false,
        user:{},
        mainCategories:[]
    },
    modules:{
        cart
    },
    mutations: {
        login(state){
            state.loggedIn = true;
        },

        logout(state){
            Auth.logoutUser();
            state.loggedIn = false;
        },

        setUser(state,user){
            state.user = user;
        },
        addAddress(state,address){
            let user = JSON.parse(localStorage.getItem('user'));
            if(!user.addresses)
                user.addresses = [];

            user.addresses.push(address);
            state.user = user;

            localStorage.setItem('user',JSON.stringify(user));
        },

        deleteAddress(state,index){
            let user = JSON.parse(localStorage.getItem('user'));
            user.addresses.splice(index,1);
            state.user = user;
            localStorage.setItem('user',JSON.stringify(user));
        },

        updateAddress(state,updateData){
            let user = JSON.parse(localStorage.getItem('user'));
            user.addresses[updateData.index] = updateData.address;
            state.user = user;
            localStorage.setItem('user',JSON.stringify(user));
        },

        addMainCategory(state,category){
            state.mainCategories = category;
        }

    },
    getters:{

        user(){
            return "Hello";
        }
    },
    actions: {
        logout(context){
            return new Promise((resolve,reject)=>{
                http.post("account/logout")
                    .then((res)=>{
                        resolve();
                        context.commit("logout");
                    },()=>{
                        reject();
                    })
            })
        },

        getCategories(context){
            return http.get("products/categories")
                .then((res)=>{
                    context.commit("addMainCategory",res.data.data);
                })
        }
    }
})
