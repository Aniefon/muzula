<?php

namespace App\Http\Controllers\User;

use App\Customer;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AccountController extends Controller
{


    public function register(RegistrationRequest $request){

        $user = User::create([
            'first_name' => $request->first_name,
            'role_id'=> Role::where("role_name",'user')->first()->id,
            "last_name"=>$request->last_name,
            "phone_no"=>$request->phone_no,
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);



        $token = JWTAuth::fromUser($user);
        return $this->success("Registration was successfull",compact('user','token'));

    }


    public function login(Request $request){
        $token = auth()->attempt($request->only(["email","password"]));

        if(!$token){
            return $this->error("Invalid username or password",null,402);
        }

        $user = User::with(["addresses","role"])->where("email",$request->email)->first();

        return $this->success("login was successful",compact("user","token"));
    }



    public function updateAccount(UpdateAccountRequest $request){

        $userId = auth()->user()->id;

        $user = User::find($userId);


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
//       $user->email = $request->email;
        $user->phone_no = $request->phone_no;




        $customer = Customer::updateOrCreate([
            'user_id'=>$userId
        ],[
            "address"=>$request->address,
        ]);


        if( $user->save()){
            $user = User::with(['addresses'])->find($userId);
            return $this->success("Account Updated successfully", compact("user","customer"));
        }

    }


    public function updatePassword(ChangePasswordRequest $request){

        $user = $request->user();

        $user->password = bcrypt($request->password);

        if($user->save())
            return $this->success("Password updated successfully");
        return $this->error("Password not updated successfully");
    }


    public function logout(){

        $token = JWTAuth::parseToken();

        $token->invalidate();

        return $this->success("logged out successfully");
    }

}
