import {http} from "../../../utils";


export default {
    state:{
        products:[],
    },

    mutations:{

        addProduct(state,product){
            if(product instanceof Array)
                state.products.push(...product);
            else
                state.products.push(product);

        }
    },


    actions:{

        getProducts(context){

            http.get("/admin/products")
                .then((res)=>{
                    context.commit("addProduct",res.data.data);
                })
        },

        addProduct(context,product){

            return new Promise((resolve,reject)=>{

                http.post("/admin/products/create",product)
                    .then((res)=>{
                        context.commit("addProduct",res.data.data);
                        resolve(res);
                    },(err)=>{
                        reject(err.response);
                    })
            })
        },


        updateProduct(context,updateData){
            return new Promise((resolve,reject)=>{

                http.post("/admin/products/update/"+updateData.id,updateData.form_data)
                    .then((res)=>{
                        // context.commit("addProduct",res.data.data);
                        resolve(res);
                    },(err)=>{
                        reject(err.response);
                    })
            })
        }
    },
}