<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{



    public function getPrimaryImageAttribute($image){
        return url('storage/'.$image);
    }

    public function category(){
        return $this->belongsTo("App\Models\Category","category_id","id");
    }


    public function images(){
        return $this->hasMany("App\Models\ProductImage","product_id");
    }

    public function orderItems(){
        return $this->hasMany("App\Models\OrderItem");
    }


    public function sales(){
        return $this->orderItems()->count();
    }


    public function scopeOfCategory(Builder $query,$categoryId){
        return $query->where("category_id",$categoryId);
    }

    public function scopeOfSubCategory(Builder $query,$subId){
        return $query->whereRaw("category_id in (select id from categories where parent_id = ? )",[$subId]);
    }


    public function scopeOfMainCategory(Builder $query,$mainId){
        return $query->whereRaw("category_id in (select id from categories where parent_id in (select id from categories where parent_id = ? ))",[$mainId]);
    }


    public function scopeOfPriceRage(Builder $query,$min,$max){
        return $query->whereRaw("(selling_price >= ? && <= ?) ||  (discount_price >= ? && <= ?)",[$min,$max,$min,$max]);
    }

    public function scopeOfProductName(Builder $query,$name){
        return $query->whereRaw("MATCH (product_name) AGAINST (?)",[$name]);
    }




}
