<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(["category","images"])->withCount("orderItems")->get();
        return $this->success(null,$products);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {


//        return response()->json($request->all());
        $product = new Product();

        $product->product_name = $request->product_name;
        $product->cost_price = $request->cost_price;
        $product->selling_price = $request->selling_price;
        $product->description = $request->description;
        $product->category_id = $request->category_id;
        $product->available_qty = $request->available_qty;
        $product->discount_price = $request->discount_price;
        $product->is_new = $request->is_new;
      //  $product->brand = $request->brand;
        $product->primary_image = '';

//
        if($product->save()){
            $this->uploadProductImages($product->id,$request->product_images);
            return $this->success("Product added",Product::with(["category","images"])->where("id",$product->id)->first());
        }
//
        return $this->error("product was not added");


    }


    private function uploadProductImages($productId,$images){

        foreach ($images as $image){

            $productImage = new ProductImage();
            $productImage->product_id = $productId;
            $productImage->file_location = $image->store("products","public");
            $productImage->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

        $product->images = ProductImage::where("product_id",$product->id)->get(["file_location","id"]);
        return $this->success("",$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product =  Product::find($id);

        $product->product_name = $request->product_name;
        $product->cost_price = $request->cost_price;
        $product->selling_price = $request->selling_price;
        $product->description = $request->description;
        $product->category_id = $request->category_id;
        $product->is_new = $request->is_new;
        $product->available_qty = $request->available_qty;
        $product->discount_price = $request->discount_price;
       // $product->brand = $request->brand;





        if($request->has("removed_images")){
            $this->deleteProductImages($request->removed_images);
        }


        if($product->save()){

            if($request->has("product_images")){
                $this->uploadProductImages($product->id,$request->product_images);
            }

            return $this->success("Product added",Product::with(["category","images"])->where("id",$product->id)->first());
        }

        return $this->error("product was not updated");
    }



    private function deleteProductImages(array $ids){

        foreach ($ids as $id){

            $image = ProductImage::find($id);
            if($image){
                Storage::disk("public")->delete(trim(parse_url($image->slide_image)["path"],'/'));
                $image->delete();
            }

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if($product){
            Storage::disk("public")->delete($product->primary_image);

            if($product->delete())
                return $this->success("Product deleted");
        }


        return $this->error("Product not deleted");


    }
}
