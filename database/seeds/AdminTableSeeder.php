<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where("role_name","admin")->first();
        $user = new User();
        $user->role_id = $role->id;
        $user->first_name = "Muzula";
        $user->last_name = "Admin";
        $user->email = "admin@muzula.com";
        $user->password = bcrypt("password");
        $user->save();



        $user2 = new User();
        $user2->role_id = $role->id;
        $user2->first_name = "Super";
        $user2->last_name = "Admin";
        $user2->email = "superadmin@muzula.com";
        $user2->password = bcrypt("password");




        $user2->save();
    }
}
