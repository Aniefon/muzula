@extends('affiliate.layout.master')
@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Link Tool</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Link Tool</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{route('affiliate.generate-link')}}">
                        @csrf
                        <div class="card-body">
                            <h4 class="card-title">Generate Link</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Enter Product Link</label>
                                <div class="col-sm-9">
                                    <input type="url" name="url" required class="form-control" id="fname" placeholder="enter product url">
                                </div>
                            </div>

                            @if(session()->has('generated-link'))
                                <div class="form-group row">

                                    <label for="cono1" class="col-sm-3 text-sm-center text-right control-label col-form-label">Your promotional is</label>

                                    <div class="col-8 text-center">
                                        <p class="p-2" style="border: solid 1px ; border-color: #e5e5e5;" id="url">{{session('generated-link')}}</p></div>
                                    <div>
                                        <button type="button" id="copy" class="btn btn-primary">copy</button>
                                    </div>

                                    {{--<label for="cono1" class="col-sm-3 text-right control-label col-form-label">Your promotional is</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                    {{--<textarea class="form-control">{{session('generated-link')}}</textarea>--}}
                                    {{--</div>--}}
                                </div>
                            @endif
                        </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary">Generate</button>
                    </div>
                </div>
                </form>
            </div>




        </div>

    </div>
    <!-- editor -->

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
    </div>

    <script>
        var copyBtn = document.querySelector("#copy");


        function copy() {
            var urlTex = document.querySelector("#url");
            var textArea = document.createElement("textarea");
            textArea.value = urlTex.textContent;
            document.body.appendChild(textArea);
            textArea.select();
            document.execCommand("Copy");
            textArea.remove();
        }

        copyBtn.addEventListener('click',function (event) {
            copy();

            copyBtn.innerText = "Copied";
            setTimeout(function () {
                copyBtn.innerText = "Copy";
            },500)
        })
    </script>
@stop
