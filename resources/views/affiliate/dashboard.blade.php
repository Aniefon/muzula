@extends('affiliate.layout.master')

@section('content')
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Dashboard</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Sales Cards  -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- Column -->
                <div class="col-md-6 col-lg-3 col-xlg-3">
                    <div class="card card-hover">
                        <div class="box bg-cyan d-flex">
                            <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                           <div class="m-2">
                               <h4 class="text-white">Total Income</h4>
                               <h4 class="text-white">&#8358;{{$income}}</h4>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-6 col-lg-3 col-xlg-3">
                    <div class="card card-hover d-flex">
                        <div class="box bg-success d-flex">
                            <h1 class="font-light text-white"><i class="mdi mdi-chart-areaspline"></i></h1>
                            <div class="m-2">
                                <h4 class="text-white">Total Payout</h4>
                                <h4 class="text-white">&#8358; 0</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-6 col-lg-3 col-xlg-3">
                    <div class="card card-hover">
                        <div class="box bg-warning d-flex">
                            <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                            <div class="m-2">
                                <h4 class="text-white">Total Views</h4>
                                <h4 class="text-white">{{$views}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-6 col-lg-3 col-xlg-4">
                    <div class="card card-hover">
                        <div class="box bg-danger d-flex">
                            <h1 class="font-light text-white"><i class="mdi mdi-border-outside"></i></h1>
                            <div class="m-2">
                                <h4 class="text-white">Total Sales</h4>
                                <h4 class="text-white">{{$sales}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->

                <!-- Column -->
                <!-- Column -->

            </div>
            <!-- ============================================================== -->
            <!-- Sales chart -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-md-flex align-items-center">
                                <div>
                                    <h4 class="card-title">Your income analysis</h4>
                                    <h5 class="card-subtitle">Overview of the last two weeks</h5>
                                </div>
                            </div>
                            <div class="row">
                                <!-- column -->
                                <div class="col-lg-12 mr-2">

                                    <div class="m-2">
                                        <canvas id="myChart"></canvas>
                                    </div>
                                    {{--<div class="flot-chart">--}}


                                        {{--<div class="flot-chart-content" id="flot-line-chart"></div>--}}
                                    {{--</div>--}}
                                </div>

                                <!-- column -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Sales chart -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Recent comment and chats -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- Recent comment and chats -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        {{--<footer class="footer text-center">--}}
            {{--All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.--}}
        {{--</footer>--}}
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
@endsection

@section('scripts')
    <script>
        var chartData = @json($chart)
    </script>
    <script src="{{asset('bus/libs/moment/moment.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="{{asset('bus/js/pages/chart/dashboard-chart.js')}}"></script>


@endsection
