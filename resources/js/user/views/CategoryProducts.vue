<template>
    <div class="main-category" v-if="category">
        <div class="container">
            <div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><router-link to="/" >Home</router-link></li>
                        <li class="breadcrumb-item"><router-link :to="getMainUrl(category.parent.parent)" >{{category.parent.parent.category_name}}</router-link></li>
                        <li class="breadcrumb-item"><router-link :to="getSubUrl(category.parent)" >{{category.parent.category_name}}</router-link></li>
                        <li class="breadcrumb-item active" aria-current="page">{{category.category_name}}</li>
                    </ol>
                </nav>
            </div>
            <div class="row py-5">
                <div class="col-md-3">
                    <aside class="sidebar">
                        <div class="head">
                            <h6 class="title">Categories</h6>
                        </div>
                        <div class="sub-cats">
                            <span class="cat parent d-block px-3 pt-2"><router-link :to="getMainUrl(category.parent.parent)">{{category.parent.parent.category_name}}</router-link></span>
                            <span class="cat parent d-block px-4"><router-link :to="getSubUrl(category.parent)">{{category.parent.category_name}}</router-link></span>
                            <!--<span class="sub cat px-5"><router-link :to="getSubUrl(category)">{{category.category_name}}</router-link></span>-->
                            <ul class="list-unstyled sub-cats-list ml-6">
                                <li v-for="(cat,index) of category.parent.sub_categories">
                                    <router-link :to="getCategoryUrl(cat)"> {{cat.category_name}}</router-link>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="col-md-9">
                    <div class="header">
                        <h4>{{category.category_name}}</h4>
                    </div>


                    <div class="content" v-if="products.data.length>0">
                        <div class="filtering">
                            <div class="row">
                                <div class="col-md-3 ml-auto">
                                    <select class="form-control form-control-sm">
                                        <option>Sort Products</option>
                                        <option>By Relevance</option>
                                        <option>Price Low To High</option>
                                        <option>Price High To Low</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div class="product-list">
                            <div class="row">
                                <div class="col-md-3 my-3" v-for="product of products.data">
                                    <product :product="product"></product>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="c-pagination">
                            <pagination :data="products" @pagination-change-page="getProducts"></pagination>
                        </div>
                    </div>
                    <div class="text-center" v-else>
                        <span class="no-content">There is currently no product in this category</span>
                    </div>


                </div>
            </div>
        </div>
    </div>
</template>

<script>
    import {http} from "../../utils";
    import Product from '../components/Product';
    import slugify from '@sindresorhus/slugify';
    import Pagination from 'laravel-vue-pagination';

    export default {
        name: "CategoryProducts",
        data:()=>({
            category:null,
            catID:null,
            products:{data:[]}
        }),
        components:{
            Product,Pagination
        },
        computed:{
            categories(){
                return this.$store.state.mainCategories;
            },
            catId(){
                return this.$route.params.id;
            }
        },

        watch:{
            categories(value){
                this.getCategory();
            },
            catId(val){
                this.catID = val;
                this.getCategory();

            }
        },
        methods:{
            getMainUrl(cat){
                return '/m-cat/'+cat.id+'/'+slugify(cat.category_name.toLowerCase())
            },


            getSubUrl(cat){
                return '/s-cat/'+cat.id+'/'+slugify(cat.category_name.toLowerCase())
            },
            getCategoryUrl(cat){
                return '/cat/'+cat.id+'/'+slugify(cat.category_name.toLowerCase())
            },
            getCategory(){
                for(let parent of this.categories){
                    for (let sub of parent.sub_categories){

                        for(let cat of sub.sub_categories){
                            if(cat.id == this.catID){
                                sub.parent = parent;
                                cat.parent = sub;
                                this.category = cat;
                            }
                        }

                    }
                }
                this.getProducts();
            },

            getProducts(page=1){
                http.get("products/category/"+this.catID+"?page="+page)
                    .then((res)=>{
                        this.products = res.data.data;

                    })
            }
        },
        created() {

            if(this.$route.params.id){
                this.catID = this.$route.params.id;
                if(this.categories.length>0){
                    this.getCategory();


                }

            }
        }
    }
</script>

<style scoped lang="scss">


    .sidebar{
        height: 100%;

        .head{
            .title{
                font-weight: bolder;
                font-size: 1.1em;
            }
        }
    }
    .sub-cats{
        background: #fff;
        height: 100%;

        a,.cat{
            font-weight: bold;
            font-size: .95em;
            display: block;
            color: darken(grey,20%);
        }

        .sub-cats-list{
            margin-left: 30px;
        }

        .router-link-active{
            font-weight: bolder;
            color: darken(grey,30%);
        }
    }

</style>