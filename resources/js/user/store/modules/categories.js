import {http} from "../../../utils";


export default {

    state:{
        mainCategories:[],
        subCategories:[],
        subSubCategories:[],

    },



    mutations:{
        addMainCategory(state,category){
            if(category instanceof Array)
                state.mainCategories.push(...category);
            else
                state.mainCategories.push(category);
        },

        addSubCategory(state,category){
            if(category instanceof Array)
                state.subCategories.push(...category);
            else
                state.subCategories.push(category);
        },


        addSubSubCategory(state,category){
            if(category instanceof Array)
                state.subSubCategories.push(...category);
            else
                state.subSubCategories.push(category);
        },








    },

    actions:{

        getMainCategories(context){
            http.get("/admin/categories/main-category/list")
                .then((res)=>{
                    context.commit("addMainCategory",res.data);
                })
        },





        getSubCategories(context){

            http.get("/admin/categories/sub-category/list")
                .then((res)=>{
                    // context.commit("clearArray","subCategories");
                    context.commit("addSubCategory",res.data);
                })
        },




        getSubSubCategories(context){
            http.get("/admin/categories/sub-sub-category/list")
                .then((res)=>{
                    // context.commit("clearArray","subCategories");
                    context.commit("addSubSubCategory",res.data);
                })
        },


        getCategories(context){
            context.dispatch("getMainCategories");
            context.dispatch("getSubCategories");
            context.dispatch("getSubSubCategories");
        }

    },



}