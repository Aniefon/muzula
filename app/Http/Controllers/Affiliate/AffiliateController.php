<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 2019-03-26
 * Time: 22:26
 */

namespace App\Http\Controllers\Affiliate;


use App\Http\Controllers\Controller;
use App\Models\AffiliateProductView;
use App\Models\AffiliateSale;
use App\Models\Bank;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;


class AffiliateController extends Controller
{



    public function __construct()
    {


        $this->middleware(function ($request, $next) {

            $user = auth()->user();

            View::share(['balance'=>$user->getBalance()]);

            return $next($request);
        });


    }

    public function showDashboard(){
        $user = auth()->user();

        $data['chart']['views'] = $this->getAffiliateSummary();
        $data['chart']['sales'] = $this->getAffiliateSales();

        $data['income'] = AffiliateSale::where('affiliate_id',$user->affiliate_id)->sum('amount');
        $data['views'] = AffiliateProductView::where('affiliate_id',$user->affiliate_id)->sum('views');
        $data['sales'] = AffiliateSale::where('affiliate_id',$user->affiliate_id)->count();
        return view('affiliate.dashboard',$data);
    }


    public function showLinkTool(){
        return view('affiliate.link-tool');
    }


    public function showBanners(){
        return view('affiliate.banners');
    }


    public function showWithdrawalDetails(){
        $user = auth()->user();
        $banks = Bank::all();

        return view('affiliate.withdrawal-details',["user"=>$user,'banks'=>$banks]);
    }

    public function showWithdrawalHistory(){
        return view('affiliate.withdrawal-history');
    }


    public function getAffiliateSummary(){

        $startDate = now()->subDay(30)->startOfDay();
        $endDate = now()->endOfDay();
        $summary = DB::select('select  viewed_date as date, sum(views) as views from affiliate_product_views where created_at between ? and ? group by viewed_date',[$startDate,$endDate]);

        return $summary;

    }

    public function getAffiliateSales(){

        $startDate = now()->subDay(30)->startOfDay();
        $endDate = now()->endOfDay();
        $summary = DB::select('select  date(created_at) as date, count(*) as sales from affiliate_sales where created_at between ? and ? group by date(created_at)',[$startDate,$endDate]);

        return $summary;

    }
}
